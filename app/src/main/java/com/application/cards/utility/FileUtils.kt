package com.application.cards.utility

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.provider.OpenableColumns
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream

object FileUtils {

    fun getFileName(context: Context?, uri: Uri?): String? {
        if (uri == null || context == null) {
            return null
        }
        var name: String? = null
        if (ContentResolver.SCHEME_CONTENT == uri.scheme) {
            context.contentResolver.query(
                uri, null, null, null,
                null
            ).use { cursor ->
                if (cursor != null && cursor.moveToFirst()) {
                    name = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            }
        }
        if (name == null) {
            name = uri.lastPathSegment
            if (name != null && name!!.contains("/")) {
                val index = name!!.lastIndexOf('/')
                name = name!!.substring(index + 1)
            }
        }
        return name
    }

    @Throws(IOException::class)
    fun inputStreamToFile(inputStream: InputStream, file: File) {
        FileOutputStream(file).use { outputStream ->
            var read: Int
            val bytes = ByteArray(inputStream.available())
            if (bytes.isEmpty()) {
                throw RuntimeException("Bytes array should not have 0 size")
            }
            while (inputStream.read(bytes).also { read = it } != -1) {
                outputStream.write(bytes, 0, read)
            }
        }
    }
}