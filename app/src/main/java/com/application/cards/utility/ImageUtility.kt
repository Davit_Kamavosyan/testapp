package com.application.cards.utility

import com.application.cards.R

object ImageUtility {

    fun getImageResId(): Int {

        val imageArrayIsList : IntArray = intArrayOf(
            R.drawable.ic_image_1,
            R.drawable.ic_image_2,
            R.drawable.ic_image_3
        )

        return imageArrayIsList.random()
    }
}