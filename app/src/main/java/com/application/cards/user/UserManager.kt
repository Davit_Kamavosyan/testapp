package com.application.cards.user

import android.content.Context
import android.content.SharedPreferences
import com.application.cards.BuildConfig

private const val IS_DATA_CREATED = "IS_DATA_CREATED"
private const val PREFS_FILE = BuildConfig.APPLICATION_ID + ".pref_file"

class UserManager(context: Context) {

    private val mSharedPreferences: SharedPreferences = context.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE)

    fun dataIsCreated() {
        val mEditor = mSharedPreferences.edit()
        mEditor.putBoolean(IS_DATA_CREATED, true)
        mEditor.apply()
    }

    fun isDataCreated(): Boolean {
        return mSharedPreferences.getBoolean(IS_DATA_CREATED, false) ?: false
    }
}