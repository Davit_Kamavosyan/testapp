package com.application.cards.presentation.adapter.callback

interface ListAction {
    fun onImageClicked(id: Long?)
    fun onTextChanged(item: ChangesItem)
}