package com.application.cards.presentation

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.application.cards.data.db.DatabaseService
import com.application.cards.presentation.usecases.UseCases
import com.application.cards.data.repository.ItemRepository
import com.application.cards.data.repository.RoomItemDataSource
import com.application.cards.data.usecase.*


class ItemViewModelFactory(private val context: Application) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ItemViewModel::class.java)) {

            val itemDao = DatabaseService.getInstance(context).itemDao()

            val repository = ItemRepository(RoomItemDataSource(itemDao))

            val useCases = UseCases(
                GenerateItem(repository),
                GetAllItems(repository),
                RemoveItem(repository),
                UpdateItemImageUrl(repository),
                UpdateItemTitle(repository)
            )

            return ItemViewModel(useCases) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
