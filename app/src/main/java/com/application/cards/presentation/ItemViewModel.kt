package com.application.cards.presentation

import androidx.lifecycle.ViewModel
import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.application.cards.presentation.usecases.UseCases
import com.application.cards.utility.ImageUtility
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ItemViewModel(private val useCases: UseCases) : ViewModel() {

    private val coroutineScope = CoroutineScope(Dispatchers.IO)

    val allCheeses = Pager(
        PagingConfig(
            pageSize = 40,
            enablePlaceholders = true,
            maxSize = 500
        )
    ) {
        useCases.getAllItems()
    }.flow

    fun addItem() {
        coroutineScope.launch {
            useCases.generateItem()
        }
    }

    fun generateItems(count:Int) {
        coroutineScope.launch {
            for (i in 1..count){
                useCases.generateItem()
            }
        }
    }

    fun updateTitle(id: Long, text: String) {
        coroutineScope.launch {
            useCases.updateItemTitle(id, text)
        }
    }

    fun updateImageUrl(id: Long, imageUrl: String) {
        coroutineScope.launch {
            useCases.updateItemImageUrl(id, imageUrl)
        }
    }
}