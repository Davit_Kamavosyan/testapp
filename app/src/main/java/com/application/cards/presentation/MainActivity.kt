package com.application.cards.presentation

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.application.cards.R
import com.application.cards.presentation.adapter.callback.ChangesItem
import com.application.cards.presentation.adapter.ItemAdapter
import com.application.cards.presentation.adapter.callback.ListAction
import com.application.cards.user.UserManager
import com.application.cards.utility.FileUtils.inputStreamToFile
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import java.io.File


class MainActivity : AppCompatActivity(){

    private val REQUEST_GET_IMAGE = 1
    private var selectedItemId = -1L
    private lateinit var userManager: UserManager
    private lateinit var viewModel: ItemViewModel
    private lateinit var adapter: ItemAdapter
    private lateinit var layoutManager:GridLayoutManager

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        userManager = UserManager(this)
        adapter = ItemAdapter()

        CoroutineScope(Dispatchers.IO).launch {
            adapter.setListeners().debounce(1000).collect {
                viewModel.updateTitle(it.id, it.text?:"")
            }
        }

        val rv = findViewById<RecyclerView>(R.id.main_recycler_view)
        layoutManager = GridLayoutManager(this, 2)
        rv.layoutManager = layoutManager
        rv.adapter = adapter

        val fabAddCard = findViewById<FloatingActionButton>(R.id.main_add_card_fab)

        fabAddCard.setOnClickListener {
            viewModel.addItem()
        }

        viewModel = let {
            ViewModelProvider(this, ItemViewModelFactory(application)).get(
                ItemViewModel::class.java
            )
        }

        subscribeObservers()

        if(!userManager.isDataCreated()){
            userManager.dataIsCreated()
            viewModel.generateItems(200)
        }
    }

    private fun subscribeObservers() {

        lifecycleScope.launch {
            viewModel.allCheeses.collectLatest {
                adapter.submitData(it)
            }
        }
    }

    @ExperimentalCoroutinesApi
    fun ItemAdapter.setListeners(): Flow<ChangesItem> = callbackFlow {

        setListener(object : ListAction {

            override fun onImageClicked(id: Long?) {
                selectedItemId = id?:-1
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_GET_IMAGE)
            }

            override fun onTextChanged(item: ChangesItem) {
                offer(item)
            }
        })

        awaitClose {
            setListener(null)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_GET_IMAGE && resultCode == RESULT_OK) {

            data?.data?.let { returnUri ->
                val inputStream = contentResolver?.openInputStream(returnUri) ?: return
                val fileName = com.application.cards.utility.FileUtils.getFileName(this, returnUri) ?: return

                val file = File(filesDir, fileName)

                inputStream.use {
                    inputStreamToFile(it, file)
                }

                if(selectedItemId!= -1L){
                    viewModel.updateImageUrl(selectedItemId, file.path)
                }
            }
        }
    }
}