package com.application.cards.presentation.usecases

import com.application.cards.data.usecase.*

data class UseCases(
    val generateItem: GenerateItem,
    val getAllItems: GetAllItems,
    val removeItem: RemoveItem,
    val updateItemImageUrl: UpdateItemImageUrl,
    val updateItemTitle: UpdateItemTitle)