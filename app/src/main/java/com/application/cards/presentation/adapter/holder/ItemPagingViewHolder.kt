package com.application.cards.presentation.adapter.holder

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.application.cards.R
import com.application.cards.data.db.ItemEntity
import com.application.cards.presentation.adapter.callback.ChangesItem
import com.application.cards.presentation.adapter.callback.ListAction
import com.bumptech.glide.Glide
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

class ItemPagingViewHolder(parent: ViewGroup, private val actions: ListAction?) :
    RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_card, parent, false)
    ), TextWatcher {

    private val editTextTitle: EditText = itemView.findViewById(R.id.item_title_edit_text)
    private val imageView = itemView.findViewById<ImageView>(R.id.item_image_view)
    private lateinit var item: ItemEntity

    @FlowPreview
    @ExperimentalCoroutinesApi
    fun bindTo(_item: ItemEntity?) {

        if (_item == null) {
            return
        }

        item = _item

        val imageUrl = if (item.imageURL.isNullOrEmpty()) {
            item.imageResID
        } else {
            item.imageURL
        }

        Glide
            .with(itemView.context)
            .load(imageUrl)
            .centerCrop()
            .placeholder(R.drawable.ic_image_1)
            .into(imageView)

        imageView.setOnClickListener {
            actions?.onImageClicked(item.id)
        }

        val title = item.title

        editTextTitle.setText(title)
        editTextTitle.addTextChangedListener(this)
    }

    @FlowPreview
    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        val id = item.id
        val newText = p0.toString()
        if (item.title != p0.toString() && id != null) {
            actions?.onTextChanged(ChangesItem(id, newText))
        }
    }

    override fun afterTextChanged(p0: Editable?) {

    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
}

