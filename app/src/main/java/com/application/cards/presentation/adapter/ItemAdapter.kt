package com.application.cards.presentation.adapter

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.application.cards.data.db.ItemEntity
import com.application.cards.presentation.adapter.callback.ListAction
import com.application.cards.presentation.adapter.holder.ItemPagingViewHolder

class ItemAdapter() : PagingDataAdapter<ItemEntity, ItemPagingViewHolder>(diffCallback) {

    private var actions: ListAction? = null

    fun setListener(_actions: ListAction?){
        actions = _actions
    }

    override fun onBindViewHolder(holder: ItemPagingViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemPagingViewHolder =
        ItemPagingViewHolder(parent, actions)

    companion object {
        /**
         * This diff callback informs the PagedListAdapter how to compute list differences when new
         * PagedLists arrive.
         * <p>
         * When you add a Cheese with the 'Add' button, the PagedListAdapter uses diffCallback to
         * detect there's only a single item difference from before, so it only needs to animate and
         * rebind a single view.
         *
         * @see DiffUtil
         */
        private val diffCallback = object : DiffUtil.ItemCallback<ItemEntity>() {
            override fun areItemsTheSame(oldItem: ItemEntity, newItem: ItemEntity): Boolean =
                oldItem == newItem

            /**
             * Note that in kotlin, == checking on data classes compares all contents, but in Java,
             * typically you'll implement Object#equals, and use it to compare object contents.
             */
            override fun areContentsTheSame(oldItem: ItemEntity, newItem: ItemEntity): Boolean =
                oldItem == newItem
        }
    }
}
