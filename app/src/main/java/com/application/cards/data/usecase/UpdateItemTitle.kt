package com.application.cards.data.usecase

import com.application.cards.data.repository.ItemRepository

class UpdateItemTitle(private val itemRepository: ItemRepository) {
    suspend operator fun invoke(id: Long,title:String) = itemRepository.updateTitle(id,title)
}