package com.application.cards.data.usecase

import com.application.cards.data.repository.ItemRepository

class UpdateItemImageUrl(private val itemRepository: ItemRepository) {
    suspend operator fun invoke(id: Long,imageUrl:String) = itemRepository.updateImageUrl(id,imageUrl)
}