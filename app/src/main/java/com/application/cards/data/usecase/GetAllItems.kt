package com.application.cards.data.usecase

import com.application.cards.data.repository.ItemRepository

class GetAllItems(private val itemRepository: ItemRepository) {
     operator fun invoke() = itemRepository.getAllItems()
}