package com.application.cards.data.repository

import com.application.cards.data.db.ItemEntity
import com.application.cards.utility.ImageUtility

class ItemRepository(private val dataSource: ItemDataSource) {

    suspend fun addItem() {
        val item = ItemEntity(title = "title", imageResID = ImageUtility.getImageResId())
        dataSource.addItemEntity(item)
    }

    suspend fun updateImageUrl(id: Long, imageUrl: String) = dataSource.updateImageUrl(id, imageUrl)

    suspend fun updateTitle(id: Long, title: String) = dataSource.updateTitle(id, title)

    suspend fun removeItem(item: ItemEntity) = dataSource.remove(item)

    fun getAllItems() = dataSource.getAll()
}