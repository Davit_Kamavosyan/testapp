package com.application.cards.data.usecase

import com.application.cards.data.db.ItemEntity
import com.application.cards.data.repository.ItemRepository

class RemoveItem(private val itemRepository: ItemRepository) {
    suspend operator fun invoke(item: ItemEntity) = itemRepository.removeItem(item)
}