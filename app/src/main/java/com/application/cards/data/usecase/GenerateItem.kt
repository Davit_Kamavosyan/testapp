package com.application.cards.data.usecase

import com.application.cards.data.repository.ItemRepository

class GenerateItem(private val itemRepository: ItemRepository) {
    suspend operator fun invoke() = itemRepository.addItem()
}