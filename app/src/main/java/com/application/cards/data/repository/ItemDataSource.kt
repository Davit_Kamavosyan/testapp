package com.application.cards.data.repository

import androidx.paging.PagingSource
import com.application.cards.data.db.ItemEntity

interface ItemDataSource {

    suspend fun addItemEntity(item: ItemEntity)

    suspend fun updateTitle(id: Long, title: String)

    suspend fun updateImageUrl(id: Long, imageUrl: String)

    suspend fun remove(item: ItemEntity)

    fun getAll(): PagingSource<Int, ItemEntity>

}