package com.application.cards.data.repository

import androidx.paging.PagingSource
import com.application.cards.data.db.ItemDao
import com.application.cards.data.db.ItemEntity

class RoomItemDataSource(private val itemDao: ItemDao) : ItemDataSource {

    override suspend fun addItemEntity(item: ItemEntity) = itemDao.addItemEntity(item)

    override suspend fun updateTitle(id: Long, title: String) = itemDao.updateItemTitle(id, title)

    override suspend fun updateImageUrl(id: Long, imageUrl: String) = itemDao.updateItemImageUrl(id, imageUrl)

    override suspend fun remove(item: ItemEntity) = itemDao.deleteItemEntity(item)

    override fun getAll(): PagingSource<Int, ItemEntity> = itemDao.getAllItemEntitiesPaging()

}