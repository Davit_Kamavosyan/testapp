package com.application.cards.data.db

import androidx.annotation.Nullable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "item")

data class ItemEntity(

    @ColumnInfo(name = "title")
    val title: String,

    @ColumnInfo(name = "image")
    val imageResID: Int,

    @Nullable
    @ColumnInfo(name = "image_url")
    val imageURL: String? = null,

    @PrimaryKey(autoGenerate = true)
    val id: Long? = null
)