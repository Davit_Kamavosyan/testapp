package com.application.cards.data.db

import androidx.paging.PagingSource
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE

@Dao
interface ItemDao {

    @Insert(onConflict = REPLACE)
    suspend fun addItemEntity(itemEntity: ItemEntity)

    @Update
    suspend fun updateCardEntity(vararg itemEntity: ItemEntity)

    @Delete
    suspend fun deleteItemEntity(itemEntity: ItemEntity)

    @Query("UPDATE item SET title = :title WHERE id = :id")
    suspend fun updateItemTitle(id: Long, title: String)

    @Query("UPDATE item SET image_url = :imageUrl WHERE id = :id")
    suspend fun updateItemImageUrl(id: Long, imageUrl: String)

    @Query("SELECT * FROM item")
    fun getAllItemEntitiesPaging(): PagingSource<Int, ItemEntity>


}